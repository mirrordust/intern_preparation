package com.mirrordust.leetcode.dp;

public class JumpGameII {

    public int jump(int[] nums) {
        int N = nums.length;
        int[] f = new int[N];
        for (int i = 0; i < N; i++) {
            f[i] = 0x7fffffff;
        }
        f[0] = 0;
        for (int i = 0; i < N - 1; i++) {
            for (int j = i + 1; j < i + nums[i] + 1; j++) {
                if (j < N) {
                    f[j] = f[j] <= f[i] + 1 ? f[j] : f[i] + 1;
                }
            }
        }
        return f[N - 1];
    }
}
