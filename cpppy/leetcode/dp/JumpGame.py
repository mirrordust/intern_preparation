# Given an array of non-negative integers, you are initially positioned at the first index of the array.
#
# Each element in the array represents your maximum jump length at that position.
#
# Determine if you are able to reach the last index.
#
# Example 1:
#
# Input: [2,3,1,1,4]
# Output: true
# Explanation: Jump 1 step from index 0 to 1, then 3 steps to the last index.
# Example 2:
#
# Input: [3,2,1,0,4]
# Output: false
# Explanation: You will always arrive at index 3 no matter what. Its maximum
# jump length is 0, which makes it impossible to reach the last index.
import time
from typing import List


class Solution:
    def canJump_reach_v2(self, nums: List[int]) -> bool:  # AC
        # 从前往后 O(n)
        N = len(nums)
        reach = 0 + nums[0]
        for i in range(1, N):
            if i > reach:  # i 大于 reach 则意味着在这个点断开了, 就算后面可以跳到终点, 前一段也到不了后一段
                return False  # 对于无法到达的输入, 可以提早结束循环
            reach = max(reach, i + nums[i])
        return True  # 没有提早结束说明必然到了终点, 不需要判断直接返回True就行了

    def canJump_reach(self, nums: List[int]) -> bool:  # AC
        # 从前往后 O(n)
        N = len(nums)
        reach = 0 + nums[0]
        for i in range(1, N):
            new_reach = i + nums[i]
            if i <= reach < new_reach:
                reach = new_reach
        return reach >= N - 1

    def canJump(self, nums: List[int]) -> bool:  # AC
        # 从后往前 O(n)
        N = len(nums)
        last = N - 1
        for i in range(N - 2, -1, -1):
            if i + nums[i] >= last:
                last = i
        return last == 0

    def canJump_v2(self, nums: List[int]) -> bool:  # TLE
        # 实际为从后向前找的思路
        # 找从末尾开始能到的最远的位置
        # 最坏时间复杂度: n^2
        # TLE: 输入为全1 [1]*25000
        def back(watch):
            far = watch
            for i in range(watch - 1, -1, -1):
                if nums[i] >= watch - i:
                    far = i
            return far

        far = len(nums) - 1
        while True:
            watch = far
            far = back(watch)
            if far == 0:
                return True
            if far == watch:
                return False

    def canJump_v1(self, nums: List[int]) -> bool:  # TLE
        # 实际为从前向后找的思路
        # 最坏时间复杂度: n^2 ==> TLE: 输入为list(range(25000, 0, -1)) + [1,0,0]
        N = len(nums)
        f = [0x7fffffff] * N
        f[0] = 0  # f[i]存储最远的能到达该位置的下标
        for length in range(1, nums[0] + 1):
            if 0 + length < N:
                f[0 + length] = 0
        for i in range(1, len(nums) - 1):
            for length in range(1, nums[i] + 1):
                if i + length < N:
                    f[i + length] = min(f[i + length], i)
        pre = f[-1]
        while True:
            if pre == 0:
                break
            if pre != 0x7fffffff:
                pre = f[pre]
            else:
                break
        if pre == 0:
            return True
        else:
            return False


if __name__ == '__main__':
    inputs = [
        [2, 3, 1, 1, 4],
        [3, 2, 1, 0, 4],
        list(range(25000, 0, -1)) + [1, 0, 0],
        [1] * 25000,
    ]
    outputs = [
        True,
        False,
        False,
        True,
    ]

    solution = Solution()

    for idx, case in enumerate(zip(inputs, outputs)):
        print(f'case: {idx}')
        start = time.time()
        in_ = case[0]
        ans = case[1]
        assert solution.canJump(in_) == ans
        print(f'time: {time.time() - start}')

# v2 time:
# case: 0
# time: 8.821487426757812e-06
# case: 1
# time: 1.5974044799804688e-05
# case: 2
# time: 0.0038530826568603516
# case: 3
# time: 26.616195917129517

# v1 time:
# case: 0
# time: 1.0013580322265625e-05
# case: 1
# time: 5.0067901611328125e-06
# case: 2
# time: 87.70088076591492
# case: 3
# time: 0.015391826629638672
