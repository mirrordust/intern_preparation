import time
from typing import List


class Solution:

    def jump(self, nums: List[int]) -> int:
        # 贪心
        # O(n)
        # 贪心解法和动态规划的区别在于: 动态规划可以找到跳到最后的路径, 而贪心解法无法提取出路径
        count = 0
        current_reachable = 0
        next_farest = 0 + nums[0]
        for i in range(len(nums)):
            if i > current_reachable:
                count += 1
                current_reachable = next_farest
            next_farest = max(next_farest, i + nums[i])

        return count  # 由于题目假设总能跳到最后,因此 current_reachable 最后一定 大于等于 len(nums)-1

    def jump_dp(self, nums: List[int]) -> int:  # TLE: case 3
        # f[i]存储jump_to(i)的最小跳数
        # 最坏时间复杂度: n^2
        f = [0x7fffffff] * len(nums)
        f[0] = 0
        for i in range(0, len(nums) - 1):
            for j in range(i + 1, i + nums[i] + 1):
                if j < len(nums):
                    f[j] = min(f[j], f[i] + 1)
        return f[-1]

    def jump_memorize(self, nums: List[int]) -> int:  # TLE: case 2
        # 记忆化搜索, f[i]存储jump_to(i)的结果
        # 对case 2超时且RecursionError: maximum recursion depth exceeded in comparison
        f = [-1] * len(nums)
        f[0] = 0

        def jump_to(i):
            # 已经计算过了则直接返回
            if f[i] != -1:
                return f[i]
            # 没计算过则第一次计算
            if i == 0:
                return 0
            n_jump_list = []
            for j in range(i - 1, -1, -1):
                if j + nums[j] >= i:
                    n_jump = jump_to(j) + 1
                    n_jump_list.append(n_jump)
            f[i] = min(n_jump_list)  # 计算结果赋值到f[i]
            return f[i]

        return jump_to(len(nums) - 1)

    def jump_recursive(self, nums: List[int]) -> int:  # TLE: case 1
        # 递归的做法: jump_to(i)表示跳到i需要的最小跳数
        # 对case 1超时
        # 对case 2直接RecursionError: maximum recursion depth exceeded in comparison
        def jump_to(i):
            if i == 0:
                return 0
            n_jump_list = []
            for j in range(i - 1, -1, -1):
                if j + nums[j] >= i:
                    n_jump = jump_to(j) + 1
                    n_jump_list.append(n_jump)
            return min(n_jump_list)

        return jump_to(len(nums) - 1)


if __name__ == '__main__':
    inputs = [
        [2, 3, 1, 1, 4],  # case 0
        [2, 9, 6, 5, 7, 0, 7, 2, 7, 9, 3, 2, 2, 5, 7,
         8, 1, 6, 6, 6, 3, 5, 2, 2, 6, 3],  # case 1,
        [1] * 25000,  # case 2
        list(range(25000, 0, -1)) + [1, 0],  # case 3
    ]
    outputs = [
        2,
        5,
        25000 - 1,
        2,  # 跳两步跳法: 25000 -> 第二个1 -> 0
    ]
    ss = {
        # 's_r': Solution().jump_recursive,
        # 's_m': Solution().jump_memorize,
        # 's_dp': Solution().jump_dp,
        's_final': Solution().jump,
    }

    for k, s in ss.items():
        print(f'method: {k}')
        start = time.time()
        for i in range(len(inputs)):
            case = inputs[i]
            ans = outputs[i]
            assert s(case) == ans
        print(f'time: {time.time() - start}')
