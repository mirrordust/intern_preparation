# 这道题的贴别之处: 由于可能没有解答, 因此递归求解可能会比较麻烦(在处理没答案的情况下)
import time
from typing import List


class Solution:
    def coinChange_dp(self, coins: List[int], amount: int) -> int:  # AC
        min_coin = min(coins)
        if 0 < amount < min_coin or min_coin < amount < 2 * min_coin:
            return -1

        # 最坏时间复杂度 O(amount*len(coins))
        coins = [coin for coin in coins if coin <= amount]  # 大于amout的硬币没用, 排除

        f = [0x7fffffff] * (amount + 1)
        f[0] = 0
        for v in coins:
            f[v] = 1
        for i in range(1, amount + 1):
            for v in coins:
                if i - v >= 0:
                    f[i] = min(f[i], f[i - v] + 1)
        if f[-1] >= 0x7fffffff:
            return -1
        else:
            return f[-1]

    def coinChange_memorize(self, coins: List[int], amount: int) -> int:  # AC
        min_coin = min(coins)
        if 0 < amount < min_coin or min_coin < amount < min_coin * 2:
            return -1
        coins = [c for c in coins if c <= amount]

        f = [-2] * (amount + 1)
        f[0] = 0
        for c in coins:
            f[c] = 1

        def least_coins_for_x(x):  # 凑出x需要的最少硬笔数
            # 已有结果
            if f[x] != -2:
                return f[x]
            # 需要计算
            count = 0x7fffffff
            for coin in coins:
                residual = x - coin
                if residual >= 0:
                    remain = least_coins_for_x(residual)
                    if remain != -1:
                        count = min(remain + 1, count)
            if count == 0x7fffffff:
                count = -1
            f[x] = count
            return count

        ret = least_coins_for_x(amount)
        if ret >= 0:
            return ret
        else:
            return -1

    def coinChange_recursive(self, coins: List[int], amount: int) -> int:  # TLE: for case 2
        can = [True]

        def least_coins_for_x(x):  # 凑出x需要的最少硬笔数
            if x == 0:
                return 0
            if x in coins:
                return 1
            if x < min(coins):
                # x 比所有硬币的面值都小, 也就没法凑齐了
                can[0] = False
                return -1

            count = 0x7fffffff
            for coin in coins:
                residual = x - coin
                if residual >= 0:
                    count = min(least_coins_for_x(residual) + 1, count)
            return count

        ret = least_coins_for_x(amount)
        if can[0]:
            return ret
        else:
            return -1


if __name__ == '__main__':
    inputs = [
        ([1, 2, 5], 11),  # case 0
        ([2], 3),  # case 1
        ([1, 2, 5], 100),  # case 2
        ([186, 419, 83, 408], 6249),  # case 3
    ]
    outputs = [
        3,
        -1,
        20,
        20,
    ]

    ss = {
        # 's_r': Solution().coinChange_recursive,
        's_m': Solution().coinChange_memorize,
        # 's_dp': Solution().coinChange_dp,
    }

    for k, s in ss.items():
        print(f'method: {k}')
        for idx, case in enumerate(zip(inputs, outputs)):
            print(f'case: {idx}')
            start = time.time()
            coins, amount = case[0]
            ans = case[1]
            myans = s(coins, amount)
            print(f'myans: {myans}')
            assert myans == ans
            print(f'time: {time.time() - start}')
