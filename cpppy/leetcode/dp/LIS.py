from typing import List


class Solution:
    def binary_search(self, arr, e):
        # 返回e应该插入(替换)的索引
        N = len(arr)
        left, right = 0, N - 1

        while left < right:  # 循环结束时一定有left==right
            mid = (left + right) // 2
            if arr[mid] < e:
                left = mid + 1
            else:  # arr[mid] >= e
                right = mid
        return left

    def lengthOfLIS(self, nums: List[int]) -> int:  # AC
        # 在v2的基础上,由于f是有序的,对for j in range(...的循环进行二分查找
        # input = [4,10,4,3,8,9], output = 3
        N = len(nums)
        if N == 0:
            return 0
        f = [nums[0]]
        for i in range(1, N):
            e = nums[i]
            if e > f[-1]:
                f.append(e)
            else:
                position = self.binary_search(f, e)
                f[position] = e
        print(f)
        return len(f)

    def lengthOfLIS_v2(self, nums: List[int]) -> int:  # AC
        # n^2 解法二
        # f 不再保存函数v1中的长度,而是: f[i]表示长度i+1的LIS的最小末尾元素
        N = len(nums)
        if N == 0:
            return 0
        f = [nums[0]]
        for i in range(1, N):
            e = nums[i]
            if e > f[-1]:
                f.append(e)
            else:
                for j in range(len(f) - 1, 0, -1):
                    if f[j - 1] < e < f[j]:
                        f[j] = e
                if e < f[0]:
                    f[0] = e
        return len(f)

    def lengthOfLIS_v1(self, nums: List[int]) -> int:  # AC
        # n^2 解法一
        N = len(nums)
        if N == 0:
            return 0
        f = [1] * N  # f[i]表示以第i个元素结尾的LIS的长度
        for i in range(N):
            for j in range(i):
                if nums[j] < nums[i]:
                    f[i] = max(f[i], f[j] + 1)
        return max(f)


if __name__ == '__main__':
    assert Solution().lengthOfLIS([10, 9, 2, 5, 3, 7, 101, 18]) == 4
    assert Solution().lengthOfLIS([4, 10, 4, 3, 8, 9]) == 3
