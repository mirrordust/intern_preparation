# 只能向下或向右移动
# m = 3, n = 2:
#     x   x
#     x   x
#     x   x
# path 1:
#     o   x
#     o   x
#     o   o
# path 2:
#     o   x
#     o   o
#     x   o
# path 3:
#     o   o
#     x   o
#     x   o
import time


class Solution:

    def uniquePaths(self, m: int, n: int) -> int:
        # TODO: 有个非dp解法, 时间复杂度min(O(m),O(n)), 空间复杂度O(1) ???
        # https://leetcode.com/problems/unique-paths/discuss/288185/DPmin(O(m)O(n))O(1)
        pass

    def uniquePaths_dp(self, m: int, n: int) -> int:
        # bottom-up 动规
        f = []
        for i in range(m):
            f.append([-1]*n)
        for i in range(m):
            for j in range(n):
                if (i, j) in [(0, 0), (0, 1), (1, 0)]:
                    f[i][j] = 1
                else:
                    c = 0
                    if i-1 >= 0:
                        c = c+f[i-1][j]
                    if j-1 >= 0:
                        c = c+f[i][j-1]
                    f[i][j] = c
        return f[-1][-1]

    def uniquePaths_memorize(self, m: int, n: int) -> int:
        # 记忆化搜索
        f = []
        for _ in range(m):
            f.append([-1]*n)

        def upaths(x, y):
            if x == 0 and y == 0:
                return 1
            if x == 0 and y == 1:
                return 1
            if x == 1 and y == 0:
                return 1
            if f[x][y] != -1:
                return f[x][y]
            c1 = 0
            c2 = 0
            if x-1 >= 0:
                c1 = upaths(x-1, y)
            if y-1 >= 0:
                c2 = upaths(x, y-1)
            f[x][y] = c1+c2
            return c1+c2

        return upaths(m-1, n-1)

    def uniquePaths_recursive(self, m: int, n: int) -> int:  # TLE
        # 递归
        def upaths(x, y):
            if x == 0 and y == 0:
                return 1
            if x == 0 and y == 1:
                return 1
            if x == 1 and y == 0:
                return 1
            c1 = 0
            c2 = 0
            if x-1 >= 0:
                c1 = upaths(x-1, y)
            if y-1 >= 0:
                c2 = upaths(x, y-1)
            return c1+c2
        return upaths(m-1, n-1)


if __name__ == '__main__':
    inputs = [
        [3, 2],  # case 0
        [7, 3],  # case 1
        [23, 12]  # case 2
    ]
    outputs = [
        3,
        28,
        193536720,
    ]

    ss = {
        # 's_r': Solution().uniquePaths_recursive,
        # 's_m': Solution().uniquePaths_memorize,
        's_dp': Solution().uniquePaths_dp,
        # 's_final': Solution().uniquePaths,
    }

    for k, s in ss.items():
        print(f'method: {k}')
        for idx, case in enumerate(zip(inputs, outputs)):
            print(f'case: {idx}')
            start = time.time()
            m, n = case[0]
            ans = case[1]
            assert s(m, n) == ans
            print(f'time: {time.time() - start}')
