// 关键：对输入进行排序后再处理
#include <vector>
#include <iostream>

using namespace std;


class Solution {
public:
    int jump(vector<int> &nums) { // O(n^2): TLE for list(range(25000, 0, -1)) + [1, 0]
        int N = nums.size();
        long *f = new long[N];
        for (int i = 0; i < N; ++i) {
            f[i] = 0x7fffffff;
        }

        f[0] = 0;
        for (int i = 0; i < N - 1; ++i) {
            for (int j = i + 1; j < i + nums[i] + 1; ++j) {
                if (j < N) {
                    f[j] = f[j] <= f[i] + 1 ? f[j] : f[i] + 1;
                }
            }
        }
        return f[N - 1];
    }
};

int main() {

    vector<int> nums = {2, 9, 6, 5, 7, 0, 7, 2, 7, 9, 3, 2, 2, 5, 7, 8, 1, 6, 6, 6, 3, 5, 2, 2, 6, 3};
    Solution solution;
    int ans = solution.jump(nums);
    // ans should be 5
    cout << ans << endl;
}
