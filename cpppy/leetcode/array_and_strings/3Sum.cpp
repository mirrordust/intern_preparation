// 关键：对输入进行排序后再处理
#include <vector>
#include <iostream>

using namespace std;

class Solution {
public:
    vector<vector<int>> threeSum(vector<int> &nums) {
        std::sort(nums.begin(), nums.end());

        vector<vector<int>> ans;

        for (int i = 0; i < nums.size(); i++) {
            int target = nums[i];
            int left = i + 1;
            int right = nums.size() - 1;
            while (left < right) {
                int sum = target + nums[left] + nums[right];
                if (sum < 0) {
                    left++;
                } else if (sum > 0) {
                    right--;
                } else {
                    vector<int> v{0, 0, 0};
                    v[0] = target;
                    v[1] = nums[left];
                    v[2] = nums[right];
                    ans.push_back(v);
                    while (left < right && nums[left] == v[1]) {
                        left++;
                    }
                    while (left < right && nums[right] == v[2]) {
                        right--;
                    }
                }
            }
            while (i + 1 < nums.size() && target == nums[i + 1]) {
                i++;
            }
        }
        return ans;
    }
};


int main() {

    vector<int> nums = {-1, 0, 1, 2, -1, -4};
    Solution solution;
    vector<vector<int>> result = solution.threeSum(nums);

    for (const vector<int> &v: result) {
        cout << v[0];
        for (int i = 1; i < v.size(); i++) {
            cout << "," << v[i];
        }
        cout << endl;
    }
}
