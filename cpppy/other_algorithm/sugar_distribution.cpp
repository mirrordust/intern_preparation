/* 记忆化搜索 */
#include <cstdio>
#include <cstring>

#define MAX 100001
#define max(a, b) (a)>(b)?(a):(b)

using namespace std;

int cal(int r, int n, int dp[], int Arr[]) {
    if (dp[r] > 0)
        return dp[r];//已经计算过
    dp[r] = 1;
    if (r + 1 <= n && Arr[r] > Arr[r + 1])//右边有人比他小，要受右边限制
        dp[r] = max(dp[r], cal(r + 1, n, dp, Arr) + 1);
    if (r - 1 >= 1 && Arr[r] > Arr[r - 1])//左边有人比他小，要受左边限制
        dp[r] = max(dp[r], cal(r - 1, n, dp, Arr) + 1);
    return dp[r];
}

int main(int argc, char *argv[]) {
    int n;
    int Arr[MAX];
    int dp[MAX];
    while (scanf("%d", &n) != EOF) {
        int i;
        memset(dp, 0, sizeof(dp));
        for (i = 1; i <= n; i++)
            scanf("%d", &Arr[i]);
        long long sum = 0;
        for (i = 1; i <= n; i++)
            sum += cal(i, n, dp, Arr);
        printf("%lld\n", sum);
    }

    return 0;
}